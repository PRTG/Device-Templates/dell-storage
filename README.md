## PRTG Device Template for monitoring Dell Storage Systems via SNMP with PRTG

This project is a custom device template that can be used to monitor **Dell Storage Systems** that implement the **DELL-STORAGE-SC-MIB** in PRTG using the auto-discovery for simplified sensor creation.

### Supported systems

According to [Dell's own documentation](https://www.dell.com/support/home/us/en/04/drivers/driversdetails?driverid=rdwfj) the following systems are supported by the **DELL-STORAGE-SC-MIB** (and as a result, should be compatible with this template):
- Dell Compellent SC4020
- Dell Compellent Series 40
- Dell Storage SC5020
- Dell Storage SC5020F
- Dell Storage SC7020
- Dell Storage SC7020F
- Dell Storage SC8000
- Dell Storage SC9000
- Dell Storage SCv2000
- Dell Storage SCv2020
- Dell Storage SCv2080
- Dell Storage SCv3000
- Dell Storage SCv3020

We're unable to guarantee compatibility with all software/firmware versions.

## Download
A zip file containing the template and necessary additional files can be downloaded from the repository using the link below:
- [Latest Version of the Template](https://gitlab.com/PRTG/Device-Templates/dell-storage/-/jobs/artifacts/master/download?job=PRTGDistZip)

## Installation Instructions
Please refer to INSTALL.md or refer to the following KB-Post:
- [Dell Compellent Monitoring](https://kb.paessler.com/en/topic/77566)

## Remaining TO-DO's
- Rename the checks and creates to be conform with the names of the objects
- Create set-up guide/instructions