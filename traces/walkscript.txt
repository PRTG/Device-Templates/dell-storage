--------
Walk Dell Storage Center
This file is compatible with the Paessler SNMP Tester
https://www.paessler.com/
--------
hrSystemUptime
walk=1.3.6.1.2.1.25.1.1
--------
MIB-2 System
walk=1.3.6.1.2.1.1
--------
entPhysicalTable (For Identification)
walk=1.3.6.1.2.1.47.1.1.1
--------
Sensor Specific Queries
----
Dell Storagecenter
walk=1.3.6.1.4.1.674.11000.2000.500.1
----